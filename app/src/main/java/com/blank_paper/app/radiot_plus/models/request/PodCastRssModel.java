package com.blank_paper.app.radiot_plus.models.request;

import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.google.gson.annotations.SerializedName;

public class PodCastRssModel {

    String title;

    @SerializedName("description")
    String description;

    @SerializedName("itunes:subtitle")
    String subTitle;

    @SerializedName("itunes:summary")
    String summary;

    @SerializedName("media:content")
    MediaContentRssModel media;

    transient String imgUrl;

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public MediaContentRssModel getMedia() {
        return media;
    }

    public String getImageUrl() {
        if (imgUrl == null) {
            imgUrl = fetchImageUrl();
        }
        return imgUrl;
    }

    public String fetchImageUrl() {
        if (description.contains("<img src=\"")) {
            int startCutPos = description.indexOf("<img src=\"") + 10;
            int endCutPos = description.indexOf("\" alt=");

            return description.substring(startCutPos, endCutPos);
        }
        return "";
    }

    public String getSubTitle() {
        return subTitle;
    }

    public PodCastDBEntity generateModelToDB() {

        PodCastDBEntity podCastDBEntity = new PodCastDBEntity();

        podCastDBEntity.setId(getTitle().hashCode());
        podCastDBEntity.setTitle(getTitle());
        podCastDBEntity.setSubtitle(getSubTitle());
        podCastDBEntity.setImgUrl(getImageUrl());
        podCastDBEntity.setSummary(getSummary());
        podCastDBEntity.setDuration(0);
        podCastDBEntity.setCurrentPosition(0);

        if (getMedia() != null) {
            podCastDBEntity.setAudioUrl(getMedia().getUrl());
        }

        return podCastDBEntity;
    }
}
