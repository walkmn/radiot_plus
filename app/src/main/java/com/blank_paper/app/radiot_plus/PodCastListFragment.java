package com.blank_paper.app.radiot_plus;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blank_paper.app.radiot_plus.adapters.PodCastAdapter;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;

import java.util.List;

public class PodCastListFragment extends BaseFragment {

    private RecyclerView rvPodCasts;
    private PodCastAdapter podCastAdapter =  new PodCastAdapter();

    public void updateList() {
        if (rvPodCasts != null) {

            List<PodCastDBEntity> podCasts = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).get().toList();
            podCastAdapter.setPodCastList(podCasts);

        }
    }

    @Override
    protected void onInit(View rootView) {
        super.onInit(rootView);

        podCastAdapter.setMainActivity(mActivity);

        rvPodCasts = (RecyclerView) findViewById(R.id.rv_pod_casts);
        rvPodCasts.setLayoutManager(new LinearLayoutManager(mActivity));
        rvPodCasts.setAdapter(podCastAdapter);

        updateList();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_podcast_list;
    }

    @Override
    protected void onMPlayerStatusChanged() {
        updateList();
    }

    public void setOnItemClickedListener(PodCastAdapter.PodCastItemClickedListener podCastItemClickedListener) {
        podCastAdapter.setOnItemClickedListener(podCastItemClickedListener);
    }
}
