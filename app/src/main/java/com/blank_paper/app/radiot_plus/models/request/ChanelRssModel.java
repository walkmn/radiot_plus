package com.blank_paper.app.radiot_plus.models.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChanelRssModel {

    @SerializedName("item")
    List<PodCastRssModel> podCasts;

    public List<PodCastRssModel> getPodCasts() {
        return podCasts;
    }
}
