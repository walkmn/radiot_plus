package com.blank_paper.app.radiot_plus.models.request;

import com.google.gson.annotations.SerializedName;

public class MediaContentRssModel {

    @SerializedName("@url")
    String url;

    public String getUrl() {
        return url;
    }
}
