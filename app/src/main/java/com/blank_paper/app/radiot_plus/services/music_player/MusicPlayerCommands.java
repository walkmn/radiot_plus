package com.blank_paper.app.radiot_plus.services.music_player;

public enum MusicPlayerCommands {
    PLAY, STOP, PAUSE, NEXT, PREV
}
