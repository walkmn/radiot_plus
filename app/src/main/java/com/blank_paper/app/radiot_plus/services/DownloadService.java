package com.blank_paper.app.radiot_plus.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.blank_paper.app.radiot_plus.R;
import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }

        int podCastId = intent.getIntExtra(MusicPlayerService.POD_CAST_ID, 0);

        final PodCastDBEntity podCastForDownload = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).where(PodCastDBEntity.ID.equal(podCastId)).get().first();

        // if file already ...
        if (podCastForDownload.getMediaLocalPath() != null && podCastForDownload.getLoadingPercent() != 0) {
            return START_STICKY;
        }

        new AsyncTask<Void, Integer, Void>() {

            String fileFullPath;

            @Override
            protected Void doInBackground(Void... voids) {
                int count;

                try {

                    URL url = new URL(podCastForDownload.getAudioUrl());
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    int lengthOfFile = connection.getContentLength();

                    InputStream input = new BufferedInputStream(url.openStream());

                    File dir = new File(getFilesDir() + "/podcasts");
                    dir.mkdir();
                    File file = new File(dir + podCastForDownload.getTitle() + ".mp3");

                    OutputStream output = new FileOutputStream(file);
                    fileFullPath = file.getAbsolutePath();

                    byte data[] = new byte[1024];

                    long total = 0;

                    int progress = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;

                        if (progress != (int) (total * 100 / lengthOfFile)) {
                            Log.d("d_progress", progress + "");

                            progress = (int) (total * 100 / lengthOfFile);

                            podCastForDownload.setLoadingPercent(progress);
                            RadioApp.getInstance().getDB().update(podCastForDownload).subscribe();

                            //publishProgress(progress);
                        }
                        output.write(data, 0, count);
                    }


                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception ignored) {
                    podCastForDownload.setLoadingPercent(0);
                    RadioApp.getInstance().getDB().update(podCastForDownload).subscribe();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (podCastForDownload.getLoadingPercent() == 100) {
                    podCastForDownload.setMediaLocalPath(fileFullPath);
                    RadioApp.getInstance().getDB().update(podCastForDownload).subscribe();
                    Toast.makeText(DownloadService.this, R.string.available_without_internet, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

        return START_STICKY;
    }
}
