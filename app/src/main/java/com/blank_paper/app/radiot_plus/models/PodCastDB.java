package com.blank_paper.app.radiot_plus.models;

import android.os.Parcelable;

import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerStatus;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Transient;

@Entity
public interface PodCastDB extends Parcelable, Persistable {

    @Key
    int getId();

    String getTitle();

    String getSubtitle();

    String getSummary();

    String getImgUrl();

    String getAudioUrl();

    int getCurrentPosition();

    int getDuration();

    int getLoadingPercent();

    String getMediaLocalPath();

    @Transient
    MusicPlayerStatus getMusicPlayerStatus();
}
