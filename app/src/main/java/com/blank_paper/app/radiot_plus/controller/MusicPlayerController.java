package com.blank_paper.app.radiot_plus.controller;

import android.content.Context;
import android.content.Intent;

import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerService;

import java.util.List;

public class MusicPlayerController {

    private Context context;

    public MusicPlayerController(Context context) {
        this.context = context;
    }

    /**
     * Process and send command to MusicPlayerService.
     *
     * @param podCastEntityId PodCast id for manage.
     * @param musicPlayerCommands Command to execute.
     * @return PodCast Id of active PodCast.
     */
    public int sendCommandToMusicPlayer(int podCastEntityId, MusicPlayerCommands musicPlayerCommands) {

        List<PodCastDBEntity> allPodCasts = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).get().toList();
        int currentIndex = getOrderNumber(allPodCasts, podCastEntityId);

        switch (musicPlayerCommands) {
            case NEXT:

                if (currentIndex != allPodCasts.size() - 1) {
                    PodCastDBEntity nextPodCast = allPodCasts.get(++currentIndex);
                    podCastEntityId = nextPodCast.getId();
                    musicPlayerCommands = MusicPlayerCommands.PLAY;
                }

                break;

            case PREV:

                if (currentIndex != 0) {
                    PodCastDBEntity prevPodCast = allPodCasts.get(--currentIndex);
                    podCastEntityId = prevPodCast.getId();
                    musicPlayerCommands = MusicPlayerCommands.PLAY;
                }

                break;

            default:
                break;
        }

        // send command and id to ser
        Intent serviceIntent = new Intent(context, MusicPlayerService.class);
        serviceIntent.putExtra(MusicPlayerService.MUSIC_PLAYER_COMMAND, musicPlayerCommands);
        serviceIntent.putExtra(MusicPlayerService.POD_CAST_ID, podCastEntityId);
        context.startService(serviceIntent);

        return podCastEntityId;
    }

    private int getOrderNumber(List<PodCastDBEntity> allPodCasts, int podCastEntityId) {
        for (int i = 0; i <= allPodCasts.size(); i++) {
            if (podCastEntityId == allPodCasts.get(i).getId()) {
                return i;
            }
        }
        return 0;
    }

}
