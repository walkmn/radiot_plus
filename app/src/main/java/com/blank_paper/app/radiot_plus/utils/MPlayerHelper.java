package com.blank_paper.app.radiot_plus.utils;

public class MPlayerHelper {

    public static String timeToFormattedString(long msTime) {
        int second = (int) (msTime / 1000);
        int minutes = second / 60;
        int lastSecond = second % 60;

        return minutes + ":" + lastSecond;
    }

    public static int getProgressPercent(long currentTime, long duration) {
        if (duration == 0) {
            return 0;
        }
        return (int) (currentTime * 100 / duration);
    }
}
