package com.blank_paper.app.radiot_plus.network;

import android.os.AsyncTask;


public class NetworkExecutor {

    private String mUrl;
    private ResponseListener responseListener;

    public NetworkExecutor setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
        return this;
    }

    public NetworkExecutor(String url) {
        this.mUrl = url;
    }

    public void execute() {
        new AsyncTask<Void, Void, NetworkBase>() {
            @Override
            protected NetworkBase doInBackground(Void... params) {

                NetworkBase networkBase = new NetworkBase(mUrl);

                return networkBase.execute();
            }

            @Override
            protected void onPostExecute(NetworkBase networkBase) {
                super.onPostExecute(networkBase);

                if (responseListener != null) {
                    if (networkBase.getResponseCode() == 200) {
                        responseListener.onSuccess(networkBase.getResponse());
                    } else {
                        responseListener.onError(networkBase.getResponseCode());
                    }
                }
            }
        }.execute();
    }
}
