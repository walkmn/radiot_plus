package com.blank_paper.app.radiot_plus;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {

    public MainActivity mActivity;

    protected View rootView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof MainActivity)) {
            throw new RuntimeException("Please add/replace this (" + this.getClass().getName() + ") fragment only to MainActivity.");
        }
        mActivity = (MainActivity) activity;
    }

    protected abstract @LayoutRes int getLayoutResource();

    protected abstract void onMPlayerStatusChanged();

    protected View findViewById(@IdRes int resourceId) {
        return rootView.findViewById(resourceId);
    }

    protected void onInit(View rootView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutResource(), container, false);

        onInit(rootView);

        return rootView;
    }
}
