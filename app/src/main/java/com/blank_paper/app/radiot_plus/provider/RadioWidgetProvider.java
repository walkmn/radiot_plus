package com.blank_paper.app.radiot_plus.provider;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.RemoteViews;

import com.blank_paper.app.radiot_plus.R;
import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerService;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class RadioWidgetProvider extends AppWidgetProvider {

    private int podCastId = 0;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        if (podCastId == 0) {
            return;
        }

        PodCastDBEntity podCastForShowing = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).where(PodCastDBEntity.ID.equal(podCastId)).get().first();

        for (int widgetId : appWidgetIds) {

            final RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.desktop_widget);

            // logo
            ImageLoader.getInstance().loadImage(podCastForShowing.getImgUrl(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    remoteViews.setImageViewBitmap(R.id.iv_logo, loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });

            // title
            remoteViews.setTextViewText(R.id.tv_title, podCastForShowing.getTitle());

            // description
            remoteViews.setTextViewText(R.id.tv_description, podCastForShowing.getSubtitle());

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        podCastId = intent.getIntExtra(MusicPlayerService.POD_CAST_ID, 0);
        super.onReceive(context, intent);
    }
}