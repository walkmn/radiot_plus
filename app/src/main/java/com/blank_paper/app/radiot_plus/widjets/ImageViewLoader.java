package com.blank_paper.app.radiot_plus.widjets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ImageViewLoader extends ImageView {

    public ImageViewLoader(Context context) {
        super(context);
    }

    public ImageViewLoader(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewLoader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void loadImage(String imgUrl) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(imgUrl, this);
    }
}
