package com.blank_paper.app.radiot_plus.services.music_player;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.blank_paper.app.radiot_plus.R;
import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.controller.NotificationController;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.provider.RadioWidgetProvider;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Main service of the program.
 *
 * Exist for playing PodCastDBEntity object oO
 */
public class MusicPlayerService extends Service implements MediaPlayer.OnPreparedListener {

    private static final String TAG = "MusicPlayerService";

    // for sending to service
    public static final String MUSIC_PLAYER_COMMAND = "music_player_command";
    public static final String POD_CAST_ID = "pod_cast_id";

    // for sending from service
    public static final String REFRESH_DATA_INTENT = "REFRESH_MUSIC_PLAYER_DATA_INTENT";

    private MediaPlayer mediaPlayer;
    private MusicPlayerStatus playerStatus = MusicPlayerStatus.STOPPED;

    private PodCastDBEntity podCastForPlay;
    private PodCastDBEntity podCastCurrent;

    private NotificationController notificationController;

    // for updating progress
    private Subscription updateTimeSubscription;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupMediaPlayer();
        notificationController = new NotificationController(this);
    }

    private void setupMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setLooping(false);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }

        int podCastId = intent.getIntExtra(POD_CAST_ID, 0);

        podCastForPlay = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).where(PodCastDBEntity.ID.equal(podCastId)).get().first();

        MusicPlayerCommands musicPlayerCommands = (MusicPlayerCommands) intent.getSerializableExtra(MUSIC_PLAYER_COMMAND);

        if (musicPlayerCommands != MusicPlayerCommands.PLAY) {
            updateTimeSubscription.unsubscribe();
        }

        // internet checking
        if (!RadioApp.getInstance().isOnline() &&
                podCastForPlay.getMediaLocalPath() == null &&
                musicPlayerCommands == MusicPlayerCommands.PLAY) {

            Toast.makeText(this, R.string.need_internet_connection, Toast.LENGTH_SHORT).show();
            return START_STICKY;
        }

        switch (musicPlayerCommands) {
            case PLAY:
                playerStatus = MusicPlayerStatus.LOADING;

                if (podCastCurrent != null && podCastCurrent.getId() == podCastForPlay.getId()) {
                    // resume
                    mediaPlayer.start();
                    playerStatus = MusicPlayerStatus.PLAYING;

                } else {
                    // play new pod cast
                    if (podCastForPlay.getAudioUrl() == null) {
                        playerStatus = MusicPlayerStatus.PLAYING;
                        Toast.makeText(this, R.string.media_not_found, Toast.LENGTH_SHORT).show();
                        sendBroadcast();
                        return START_STICKY;
                    }

                    if (podCastCurrent != null) {
                        podCastCurrent.setMusicPlayerStatus(MusicPlayerStatus.STOPPED);
                        RadioApp.getInstance().getDB().update(podCastCurrent).subscribe();
                    }

                    mediaPlayer.reset();
                    try {
                        // if we have saved data -> play from file system
                        if (podCastForPlay.getMediaLocalPath() == null) {
                            mediaPlayer.setDataSource(podCastForPlay.getAudioUrl());
                        } else {
                            mediaPlayer.setDataSource(podCastForPlay.getMediaLocalPath());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mediaPlayer.prepareAsync();
                }

                if (updateTimeSubscription != null) {
                    updateTimeSubscription.unsubscribe();
                }
                // if playing music -> send each second our pod-cast status
                updateTimeSubscription = Observable.interval(1, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.newThread())
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                if (!mediaPlayer.isPlaying()) {
                                    mediaPlayer.start();
                                }
                                sendBroadcast();
                            }
                        });

                break;

            case STOP:
                mediaPlayer.stop();
                podCastCurrent = null;
                playerStatus = MusicPlayerStatus.STOPPED;
                break;

            case PAUSE:
                mediaPlayer.pause();
                playerStatus = MusicPlayerStatus.PAUSED;
                break;

            default:
                break;
        }

        sendBroadcast();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        podCastCurrent = podCastForPlay;
        playerStatus = MusicPlayerStatus.PLAYING;

        sendBroadcast();
    }

    // send to all ...
    private void sendBroadcast() {
        Log.d(TAG, "sendBroadcast");

        // save pod cast state to data base
        podCastForPlay.setMusicPlayerStatus(playerStatus);
        podCastForPlay.setCurrentPosition(mediaPlayer.getCurrentPosition());
        podCastForPlay.setDuration(mediaPlayer.getDuration() > 0 ? mediaPlayer.getDuration() : 0);
        RadioApp.getInstance().getDB().update(podCastForPlay).subscribe();

        // create or update notification
        notificationController.makeNotification(podCastForPlay);

        // update desktop widget
        Intent intent = new Intent(this, RadioWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int widgetIDs[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), RadioWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIDs);
        intent.putExtra(POD_CAST_ID, podCastForPlay.getId());
        sendBroadcast(intent);

        // send to other
        sendBroadcast(new Intent(REFRESH_DATA_INTENT).putExtra(POD_CAST_ID, podCastForPlay.getId()));
    }
}
