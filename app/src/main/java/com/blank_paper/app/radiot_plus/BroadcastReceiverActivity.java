package com.blank_paper.app.radiot_plus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;

import com.blank_paper.app.radiot_plus.services.DownloadService;
import com.blank_paper.app.radiot_plus.services.RssSnifferService;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerService;

public abstract class BroadcastReceiverActivity extends AppCompatActivity {

    private MusicStatusUpdateReceiver musicStatusUpdateReceiver;
    private DataUpdateReceiver dataUpdateReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null) {
            dataUpdateReceiver = new DataUpdateReceiver();
        }
        if (musicStatusUpdateReceiver == null) {
            musicStatusUpdateReceiver = new MusicStatusUpdateReceiver();
        }
        registerReceiver(dataUpdateReceiver, new IntentFilter(RssSnifferService.REFRESH_DATA_INTENT));
        registerReceiver(musicStatusUpdateReceiver, new IntentFilter(MusicPlayerService.REFRESH_DATA_INTENT));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null) {
            unregisterReceiver(dataUpdateReceiver);
        }
        if (musicStatusUpdateReceiver != null) {
            unregisterReceiver(musicStatusUpdateReceiver);
        }
    }

    protected void updateRssData() {
        Intent serviceIntent = new Intent(this, RssSnifferService.class);
        serviceIntent.putExtra(RssSnifferService.COMMAND_MAKE_REQUEST, true);
        startService(serviceIntent);
    }

    public void startDownloadingPodCast(int podCastEntityId) {
        Intent serviceIntent = new Intent(this, DownloadService.class);
        serviceIntent.putExtra(MusicPlayerService.POD_CAST_ID, podCastEntityId);
        startService(serviceIntent);
    }

    public int sendCommandToMusicPlayer(int podCastEntityId, MusicPlayerCommands musicPlayerCommands) {

        if (musicPlayerCommands == MusicPlayerCommands.PLAY) {
            startDownloadingPodCast(podCastEntityId);
        }

        return RadioApp.getInstance().getMusicPlayerController().sendCommandToMusicPlayer(podCastEntityId, musicPlayerCommands);
    }

    // database
    protected abstract void onDBaseHasChanged();

    // music player
    protected abstract void onMPlayerStatusChanged(int podCastId);

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(RssSnifferService.REFRESH_DATA_INTENT)) {
                onDBaseHasChanged();
            }
        }
    }

    private class MusicStatusUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MusicPlayerService.REFRESH_DATA_INTENT)) {
                int podCastId = intent.getIntExtra(MusicPlayerService.POD_CAST_ID, 0);

                onMPlayerStatusChanged(podCastId);
            }
        }
    }
}
