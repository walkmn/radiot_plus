package com.blank_paper.app.radiot_plus.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkBase {

    private String response = "";
    private String requestURL;
    private int responseCode;

    private HttpURLConnection conn;
    private InputStream inputStream;

    public NetworkBase(String requestURL) {
        this.requestURL = requestURL;
    }


    public NetworkBase execute() {
        URL url;
        try {
            url = new URL(requestURL);

            conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(true);
            HttpURLConnection.setFollowRedirects(true);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("GET");

            Log.d("network url", requestURL);

            responseCode = conn.getResponseCode();

            inputStream = conn.getInputStream();

            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }

            response = builder.toString();
            Log.d("network receive", response);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            conn.disconnect();
        }

        return this;
    }

    public String getResponse() {
        return response;
    }

    public int getResponseCode() {
        return responseCode;
    }
}
