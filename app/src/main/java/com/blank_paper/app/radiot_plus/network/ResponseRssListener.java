package com.blank_paper.app.radiot_plus.network;

import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public abstract class ResponseRssListener<T> implements ResponseListener {

    final Class<?> typeParameterClass;
    private final GsonXml gsonXml;

    public ResponseRssListener(Class<?> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;

        XmlParserCreator parserCreator = new XmlParserCreator() {
            @Override
            public XmlPullParser createParser() {
                try {
                    return XmlPullParserFactory.newInstance().newPullParser();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        gsonXml = new GsonXmlBuilder()
                .setXmlParserCreator(parserCreator)
                .setSameNameLists(true)
                .create();
    }

    public abstract void onSuccess(T ResponseModel);

    @Override
    public void onSuccess(String xmlResult) {

        Object baseRssModel = gsonXml.fromXml(xmlResult, typeParameterClass);

        if (baseRssModel != null) {
            onSuccess((T) baseRssModel);
        }
    }
}
