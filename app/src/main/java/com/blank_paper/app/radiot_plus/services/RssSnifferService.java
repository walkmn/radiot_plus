package com.blank_paper.app.radiot_plus.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.models.request.PodCastRssModel;
import com.blank_paper.app.radiot_plus.models.request.RadioTRssModel;
import com.blank_paper.app.radiot_plus.network.RadioAPI;
import com.blank_paper.app.radiot_plus.network.ResponseRssListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RssSnifferService extends Service {

    private String TAG = "RSS_SNIFF";

    public static final String COMMAND_MAKE_REQUEST = "makeRequest";

    public static final String REFRESH_DATA_INTENT = "REFRESH_DATA_INTENT";

    @Override
    public void onCreate() {
        super.onCreate();

        // grab a new pod cast each 15 minutes
        Observable.interval(15, TimeUnit.MINUTES)
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        makeRequest();
                    }
                });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }

        boolean makeRequest = intent.getBooleanExtra(COMMAND_MAKE_REQUEST, false);

        if (makeRequest) {
            makeRequest();
        }

        return START_STICKY;
    }

    private void makeRequest() {
        Log.d(TAG, "make request...");

        new RadioAPI()
                .getPodCasts()
                .setResponseListener(new ResponseRssListener<RadioTRssModel>(RadioTRssModel.class) {
                    @Override
                    public void onError(int codeError) {

                    }

                    @Override
                    public void onSuccess(RadioTRssModel model) {

                        ArrayList<PodCastDBEntity> podCastDBEntities = new ArrayList<>();

                        for (PodCastRssModel podCastRssModel : model.getChannel().getPodCasts()) {
                            Log.d(TAG, podCastRssModel.getTitle());

                            podCastDBEntities.add(podCastRssModel.generateModelToDB());
                        }

                        // save to data base
                        RadioApp.getInstance().getDB().upsert(podCastDBEntities).subscribe(new Subscriber<Iterable<PodCastDBEntity>>() {
                            @Override
                            public void onCompleted() {
                                sendBroadcast(new Intent(REFRESH_DATA_INTENT));
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Iterable<PodCastDBEntity> podCastDBEntities) {

                            }
                        });
                    }
                }).execute();
    }
}
