package com.blank_paper.app.radiot_plus.network;

public class RadioAPI {

    private static final String URL_RSS = "http://feeds.rucast.net/radio-t";

    public NetworkExecutor getPodCasts() {
        return new NetworkExecutor(URL_RSS);
    }
}
