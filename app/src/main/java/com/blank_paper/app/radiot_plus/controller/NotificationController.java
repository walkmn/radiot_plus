package com.blank_paper.app.radiot_plus.controller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.blank_paper.app.radiot_plus.R;
import com.blank_paper.app.radiot_plus.RadioApp;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;
import com.blank_paper.app.radiot_plus.utils.MPlayerHelper;

public class NotificationController implements NotificationClickListener {

    private static final int NOTIFICATION_ID = 14273;

    private Service service;
    private MusicPlayerController musicPlayerController;
    private NotificationManager mNotificationManager;
    private Notification notification;
    private RemoteViews remoteViews;

    private PodCastDBEntity podCastDBEntity;

    public NotificationController(Service service) {
        this.service = service;
        musicPlayerController = RadioApp.getInstance().getMusicPlayerController();

        mNotificationManager = (NotificationManager) this.service.getSystemService(Context.NOTIFICATION_SERVICE);

        remoteViews = new RemoteViews(this.service.getPackageName(),
                R.layout.notification_widget);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.service)
                .setSmallIcon(R.drawable.ic_play_circle_outline_black_48dp)
                .setCustomContentView(setupCustomPlayerView(this))
                //.setOngoing(true)
                .setOnlyAlertOnce(true)
                .setAutoCancel(false);

        notification = builder.build();
    }

    public void makeNotification(final PodCastDBEntity podCastDBEntity) {
        this.podCastDBEntity = podCastDBEntity;

        remoteViews.setImageViewResource(R.id.iv_play, podCastDBEntity.getMusicPlayerStatus().imageResource);
        remoteViews.setTextViewText(R.id.tv_current_position, MPlayerHelper.timeToFormattedString(podCastDBEntity.getCurrentPosition()));
        remoteViews.setTextViewText(R.id.tv_duration, MPlayerHelper.timeToFormattedString(podCastDBEntity.getDuration()));

        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private RemoteViews setupCustomPlayerView(final NotificationClickListener notificationClickListener) {

        service.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                notificationClickListener.onPlayClicked();
            }
        }, new IntentFilter("broadcast_command_play"));

        service.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                notificationClickListener.onNextClicked();
            }
        }, new IntentFilter("broadcast_command_next"));

        service.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                notificationClickListener.onPrevClicked();
            }
        }, new IntentFilter("broadcast_command_prev"));

        service.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                notificationClickListener.onStopClicked();
            }
        }, new IntentFilter("broadcast_command_stop"));

        PendingIntent piPlay = PendingIntent.getBroadcast(service, 0,
                new Intent("broadcast_command_play"), 0);
        PendingIntent piNext = PendingIntent.getBroadcast(service, 0,
                new Intent("broadcast_command_next"), 0);
        PendingIntent piPrev = PendingIntent.getBroadcast(service, 0,
                new Intent("broadcast_command_prev"), 0);
        PendingIntent piStop = PendingIntent.getBroadcast(service, 0,
                new Intent("broadcast_command_stop"), 0);

        remoteViews.setOnClickPendingIntent(R.id.iv_play, piPlay);
        remoteViews.setOnClickPendingIntent(R.id.iv_prev, piPrev);
        remoteViews.setOnClickPendingIntent(R.id.iv_next, piNext);
        remoteViews.setOnClickPendingIntent(R.id.iv_stop, piStop);

        return remoteViews;
    }

    @Override
    public void onPlayClicked() {
        musicPlayerController.sendCommandToMusicPlayer(podCastDBEntity.getId(), podCastDBEntity.getMusicPlayerStatus().clickedCommand);
    }

    @Override
    public void onNextClicked() {
        musicPlayerController.sendCommandToMusicPlayer(podCastDBEntity.getId(), MusicPlayerCommands.NEXT);
    }

    @Override
    public void onPrevClicked() {
        musicPlayerController.sendCommandToMusicPlayer(podCastDBEntity.getId(), MusicPlayerCommands.PREV);
    }

    @Override
    public void onStopClicked() {
        musicPlayerController.sendCommandToMusicPlayer(podCastDBEntity.getId(), MusicPlayerCommands.STOP);

        // why do not work ???
        mNotificationManager.cancel(NOTIFICATION_ID);
    }
}

interface NotificationClickListener {
    void onPlayClicked();
    void onNextClicked();
    void onPrevClicked();
    void onStopClicked();
}
