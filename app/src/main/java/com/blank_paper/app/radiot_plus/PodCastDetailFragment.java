package com.blank_paper.app.radiot_plus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;
import com.blank_paper.app.radiot_plus.utils.MPlayerHelper;
import com.blank_paper.app.radiot_plus.widjets.ImageViewLoader;

public class PodCastDetailFragment extends BaseFragment {

    private int podCastEntityId = 0;
    private static final String POD_CAST_ID = "podCastEntityId";

    public void setPodCastEntityId(int podCastEntityId) {
        this.podCastEntityId = podCastEntityId;
    }

    public int getPodCastEntityId() {
        return podCastEntityId;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            podCastEntityId = savedInstanceState.getInt(POD_CAST_ID);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(POD_CAST_ID, podCastEntityId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onInit(View rootView) {
        super.onInit(rootView);

        updateUI();
    }

    public void updateUI() {

        if (podCastEntityId == 0) {
            throw new RuntimeException("You must pass 'podCastEntityId' to this fragment, so use 'setPodCastEntityId' method.");
        }

        final PodCastDBEntity podCastEntity = RadioApp.getInstance().getDB().select(PodCastDBEntity.class).where(PodCastDBEntity.ID.equal(podCastEntityId)).get().first();

        if (podCastEntity == null) {
            return;
        }

        ImageViewLoader ivImage = (ImageViewLoader) findViewById(R.id.iv_image);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        TextView tvSubtitle = (TextView) findViewById(R.id.tv_subtitle);
        TextView tvTiming = (TextView) findViewById(R.id.tv_timing);
        ImageView ivPlay = (ImageView) findViewById(R.id.iv_play);
        ImageView ivPrev = (ImageView) findViewById(R.id.iv_prev);
        ImageView ivNext = (ImageView) findViewById(R.id.iv_next);
        ProgressBar pbPlayerProgress = (ProgressBar) findViewById(R.id.pb_player_progress);

        ivImage.loadImage(podCastEntity.getImgUrl());
        tvTitle.setText(podCastEntity.getTitle());
        tvSubtitle.setText(podCastEntity.getSummary());

        // progress bar - playing media player, download status
        int progress = MPlayerHelper.getProgressPercent(
                podCastEntity.getCurrentPosition(),
                podCastEntity.getDuration()
        );
        Log.d("getLoadingPercent", podCastEntity.getLoadingPercent() + "");
        pbPlayerProgress.setProgress(progress);
        pbPlayerProgress.setSecondaryProgress(podCastEntity.getLoadingPercent());

        // timing
        String sCurrentTime = MPlayerHelper.timeToFormattedString(podCastEntity.getCurrentPosition());
        String sDurationTime = MPlayerHelper.timeToFormattedString(podCastEntity.getDuration());
        String sTiming = sCurrentTime + " / " + sDurationTime;
        tvTiming.setText(sTiming);

        // play button
        if (podCastEntity.getMusicPlayerStatus() != null) {
            ivPlay.setImageResource(podCastEntity.getMusicPlayerStatus().imageResource);
        } else {
            ivPlay.setImageResource(R.drawable.ic_play_circle_outline_black_48dp);
        }
        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (podCastEntity.getMusicPlayerStatus() != null) {
                    mActivity.sendCommandToMusicPlayer(podCastEntity.getId(), podCastEntity.getMusicPlayerStatus().clickedCommand);
                } else {
                    mActivity.sendCommandToMusicPlayer(podCastEntity.getId(), MusicPlayerCommands.PLAY);
                }
            }
        });
        ivPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                podCastEntityId = mActivity.sendCommandToMusicPlayer(podCastEntity.getId(), MusicPlayerCommands.PREV);
            }
        });
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                podCastEntityId = mActivity.sendCommandToMusicPlayer(podCastEntity.getId(), MusicPlayerCommands.NEXT);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_podcast_detail;
    }

    @Override
    protected void onMPlayerStatusChanged() {
        if (isVisible()) {
            updateUI();
        }
    }
}
