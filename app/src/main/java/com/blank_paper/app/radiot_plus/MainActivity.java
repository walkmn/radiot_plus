package com.blank_paper.app.radiot_plus;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.blank_paper.app.radiot_plus.adapters.PodCastAdapter;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;

public class MainActivity extends BroadcastReceiverActivity implements PodCastAdapter.PodCastItemClickedListener {

    private static final String IS_DETAIL_OPENED = "is_detail_opened";
    private static final String POD_CAST_ID = "pod_cast_id";

    private PodCastListFragment podCastListFragment = new PodCastListFragment();
    private PodCastDetailFragment podCastDetailFragment = new PodCastDetailFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, podCastListFragment)
                .commit();
        podCastListFragment.setOnItemClickedListener(this);

        // check a new data
        updateRssData();

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.getBoolean(IS_DETAIL_OPENED)) {
            showDetail(
                    savedInstanceState.getInt(POD_CAST_ID)
            );
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_DETAIL_OPENED, podCastDetailFragment.isVisible());
        outState.putInt(POD_CAST_ID, podCastDetailFragment.getPodCastEntityId());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDBaseHasChanged() {
        podCastListFragment.updateList();
    }

    @Override
    protected void onMPlayerStatusChanged(int podCastId) {
        podCastListFragment.onMPlayerStatusChanged();
        podCastDetailFragment.onMPlayerStatusChanged();
    }

    @Override
    public void onDetailClicked(PodCastDBEntity podCastEntity) {
        showDetail(podCastEntity.getId());
    }

    private void showDetail(int podCastEntityId) {
        podCastDetailFragment.setPodCastEntityId(podCastEntityId);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (podCastDetailFragment.isVisible()) {
                podCastDetailFragment.updateUI();
                return;
            }
            fragmentTransaction
                    .replace(R.id.container_detail, podCastDetailFragment)
                    .commit();
        } else {
            fragmentTransaction.addToBackStack(null)
                    .setCustomAnimations(
                            R.anim.slide_from_right,
                            R.anim.slide_to_right,
                            android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right
                    )
                    .replace(R.id.container, podCastDetailFragment)
                    .commit();
        }

    }
}
