package com.blank_paper.app.radiot_plus.adapters;

import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blank_paper.app.radiot_plus.MainActivity;
import com.blank_paper.app.radiot_plus.R;
import com.blank_paper.app.radiot_plus.models.PodCastDBEntity;
import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;
import com.blank_paper.app.radiot_plus.widjets.ImageViewLoader;

import java.util.ArrayList;
import java.util.List;

public class PodCastAdapter extends RecyclerView.Adapter<PodCastAdapter.PodCastViewHolder> {

    private List<PodCastDBEntity> podCastList = new ArrayList<>();
    private PodCastItemClickedListener onItemClickedListener;
    private MainActivity mainActivity;

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void setPodCastList(List<PodCastDBEntity> podCastList) {
        this.podCastList = podCastList;
        notifyDataSetChanged();
    }

    public void setOnItemClickedListener(PodCastItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    @Override
    public PodCastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pod_cast, parent, false);
        return new PodCastViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PodCastViewHolder holder, int position) {

        final PodCastDBEntity podCastDBEntity = podCastList.get(position);

        holder.tvTitle.setText(podCastDBEntity.getTitle());
        holder.tvDescription.setText(podCastDBEntity.getSubtitle());
        holder.ivImage.loadImage(podCastDBEntity.getImgUrl());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickedListener.onDetailClicked(podCastDBEntity);
            }
        });

        // play button
        if (podCastDBEntity.getMusicPlayerStatus() != null) {
            holder.ivPlay.setImageResource(podCastDBEntity.getMusicPlayerStatus().imageResource);
        } else {
            holder.ivPlay.setImageResource(R.drawable.ic_play_circle_outline_black_48dp);
        }

        holder.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (podCastDBEntity.getMusicPlayerStatus() != null) {

                    mainActivity.sendCommandToMusicPlayer(podCastDBEntity.getId(), podCastDBEntity.getMusicPlayerStatus().clickedCommand);

                } else {
                    mainActivity.sendCommandToMusicPlayer(podCastDBEntity.getId(), MusicPlayerCommands.PLAY);
                }
            }
        });


        if (position == 0 && mainActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            onItemClickedListener.onDetailClicked(podCastDBEntity);
        }
    }

    @Override
    public int getItemCount() {
        return podCastList.size();
    }

    public interface PodCastItemClickedListener {
        void onDetailClicked(PodCastDBEntity podCastEntity);
    }

    public static class PodCastViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvDescription;
        ImageViewLoader ivImage;
        ImageView ivPlay;

        public PodCastViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            ivImage = (ImageViewLoader) itemView.findViewById(R.id.iv_image);
            ivPlay = (ImageView) itemView.findViewById(R.id.iv_play);
        }
    }
}
