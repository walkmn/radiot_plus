package com.blank_paper.app.radiot_plus.network;

public interface ResponseListener {
    void onSuccess(String xmlResult);
    void onError(int codeError);
}
