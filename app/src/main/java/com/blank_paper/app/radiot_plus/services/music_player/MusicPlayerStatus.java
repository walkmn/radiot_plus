package com.blank_paper.app.radiot_plus.services.music_player;

import android.support.annotation.DrawableRes;

import com.blank_paper.app.radiot_plus.R;

public enum MusicPlayerStatus {
    PLAYING(R.drawable.ic_pause_circle_outline_black_48dp, MusicPlayerCommands.PAUSE),
    STOPPED(R.drawable.ic_play_circle_outline_black_48dp, MusicPlayerCommands.PLAY),
    PAUSED(R.drawable.ic_play_circle_outline_black_48dp, MusicPlayerCommands.PLAY),
    LOADING(R.drawable.ic_loop_black_48dp, MusicPlayerCommands.PAUSE);

    MusicPlayerStatus(int imageResource, MusicPlayerCommands clickedCommand) {
        this.imageResource = imageResource;
        this.clickedCommand = clickedCommand;
    }

    public @DrawableRes int imageResource;
    public MusicPlayerCommands clickedCommand;
}
