package com.blank_paper.app.radiot_plus;

import com.blank_paper.app.radiot_plus.utils.MPlayerHelper;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RadioHelperTest {

    @Test
    public void checkHelper_getProgressPercent() {
        long duration = 200;
        long current = 20;

        int expectedValue = 10;

        int actual = MPlayerHelper.getProgressPercent(current, duration);

        assertEquals("Filed, check function MPlayerHelper.getProgressPercent.", expectedValue, actual, 1);
    }

    @Test
    public void checkHelper_timeToFormattedString() {

        long msTime = 20000;

        String expectedValue = "0:20";

        String actual = MPlayerHelper.timeToFormattedString(msTime);

        assertEquals("Filed, check function MPlayerHelper.timeToFormattedString.", expectedValue, actual);
    }
}
