package com.blank_paper.app.radiot_plus;

import android.content.pm.PackageInfo;
import android.test.ApplicationTestCase;
import android.test.MoreAsserts;

import com.blank_paper.app.radiot_plus.services.music_player.MusicPlayerCommands;

public class RadioAppTest extends ApplicationTestCase<RadioApp> {

    RadioApp radioApp;

    public RadioAppTest() {
        super(RadioApp.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        radioApp = (RadioApp) getApplication();

    }

    public void testCorrectVersion() throws Exception {
        PackageInfo info = radioApp.getPackageManager().getPackageInfo(radioApp.getPackageName(), 0);
        assertNotNull(info);
        MoreAsserts.assertMatchesRegex("\\d\\.\\d", info.versionName);
    }

    public void testMusicController() {
        int expectedId = -2142710288;
        int actualId = radioApp.getMusicPlayerController().sendCommandToMusicPlayer(-2142710289, MusicPlayerCommands.NEXT);

        assertEquals("Some strange in MusicPlayerController class, please check it", expectedId, actualId);
    }
}